#### 1.可能需要的依赖库
| 库名|版本|作用|
|----|----|----|
|craco|   |修改打包配置|
|ant-design|||
|react-lottie-player|||
|@tailwindcss/postcss7-compat && tailwindcss||在类名里直接使用css|
|mobx && mobx-react|||
|axios|||
|sass支持（脚手架已经支持了 sass-loader node-sass 只需要安装这两个）|||
|react-refresh||热更新|   
|griffith||视频播放| 
|fs-extra||文件操作| 